import React from 'react';
import ReactDOM from 'react-dom';
import Cinema from './Cinema';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Cinema />, document.getElementById('root'));

serviceWorker.unregister();
