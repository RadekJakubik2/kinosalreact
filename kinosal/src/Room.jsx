import React, { Component } from "react";
import "./App.css";
import Seats from "./Seats";
import Films from "./Films";
import EditableLabel from 'react-editable-label';
import PropTypes from 'prop-types';

class Room extends Component {
  onClickBookSeat = (room_id, seat_id) => {
    const { getRooms, updateRooms } = this.props;
    let rooms = [...getRooms()];
    let room = rooms.find(r => r._id === room_id);
    let seat = room.seats.find(s => s._id === seat_id);
    seat.reserved = !seat.reserved;
    updateRooms(rooms, room, "seats", seat, "reserved");
  }

  onDeleteFilm = (room_id, film_id) => {
    const { getRooms, updateRooms } = this.props;
    let rooms = [...getRooms()];
    let room = rooms.find(r => r._id === room_id);
    let film = room.films.find(f => f._id === film_id);
    room.films = room.films.filter(f => f._id !== film_id);
    updateRooms(rooms, room, "films", film, undefined, "delete");
  }

  editFilmTitle = (room_id, film_id, value) => {
    const { getRooms, updateRooms } = this.props;
    let rooms = [...getRooms()];
    let room = rooms.find(r => r._id === room_id);
    let film = room.films.find(f => f._id === film_id);
    film.title = value;
    updateRooms(rooms, room, "films", film, "title");
  }

  editFilmDescription = (room_id, film_id, value) => {
    const { getRooms, updateRooms } = this.props;
    let rooms = [...getRooms()];
    let room = rooms.find(r => r._id === room_id);
    let film = room.films.find(f => f._id === film_id);
    film.description = value;
    updateRooms(rooms, room, "films", film, "description");
  }

  render() {
    const { room, editName, onDelete } = this.props;
    const deleteBtnLabel = "Delete " + room.name;
    return (
      <div>
        <h2>
          <EditableLabel
            initialValue={room.name}
            save={value => {
              if (room.name !== value) {
                console.log(`Saving '${value}'`);
                editName(room._id, value);
              }
            }}
          />
        </h2>
        <button onClick={() => onDelete(room._id)}>{deleteBtnLabel}</button>
        <Films
          room={room}
          onDelete={this.onDeleteFilm}
          editTitle={this.editFilmTitle}
          editDescription={this.editFilmDescription}
        />
        <Seats
          room={room}
          onClick={this.onClickBookSeat}
        />
      </div>
    );
  }
}

Room.propTypes = {
  room: PropTypes.object,
  getRooms: PropTypes.func,
  updateRooms: PropTypes.func,
  editName: PropTypes.func,
  onDelete: PropTypes.func
};

export default Room;