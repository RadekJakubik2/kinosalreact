import React, { Component } from "react";
import Film from "./Film";
import "./App.css";
import uuid from 'uuid';
import PropTypes from 'prop-types';

class Films extends Component {
  render() {
    const { room, onDelete, editTitle, editDescription } = this.props;
    return (
      <div>
        {room.films.map(film => (
          <Film
            key={uuid.v4()}
            film={film}
            room={room}
            onDelete={onDelete}
            editTitle={editTitle}
            editDescription={editDescription}
          />
        ))}
      </div>
    );
  }
}

Films.propTypes = {
  room: PropTypes.object,
  onDelete: PropTypes.func,
  editTitle: PropTypes.func,
  editDescription: PropTypes.func
};

export default Films;