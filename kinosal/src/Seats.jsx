import React, { Component } from "react";
import Seat from "./Seat";
import "./App.css";
import uuid from 'uuid';
import PropTypes from 'prop-types';

class Seats extends Component {
  render() {
    const { room, onClick } = this.props;
    return (
      <div>
        {room.seats.map(seat => (
          <Seat
            key={uuid.v4()}
            seat={seat}
            room={room}
            onClick={onClick}
          />
        ))}
      </div>
    );
  }
}

Seats.propTypes = {
  room: PropTypes.object,
  onClick: PropTypes.func
};

export default Seats;