import React, { Component } from "react";
import Room from "./Room";
import "./App.css";
import uuid from 'uuid';
import axios from 'axios';
const filmData = require('./initialData/filmData.json');

class Cinema extends Component {
  state = {
    rooms: []
  };

  initializeSeats(count) {
    let seats = [];
    for (let a = 0; a < count; a++) {
      seats.push({ name: a.toString(), reserved: false });
    }
    seats.find(s => s.name === "0").reserved = true;
    return seats;
  }

  componentDidMount() {
    if (process.env.REACT_APP_DB_TYPE === process.env.REACT_APP_MONGO) {
      console.log("MONGO root url");
      this.rootUrl = process.env.REACT_APP_ROOT_URL_MONGO;

      this.seedForEmptyMongo = [
        { _id: 1, name: "Kinosál 1", seats: this.initializeSeats(10), films: [...filmData] },
        { _id: 2, name: "Kinosál 2", seats: this.initializeSeats(40), films: [...filmData] },
        { _id: 3, name: "Kinosál 3", seats: this.initializeSeats(33), films: [...filmData] }
      ];
    }
    else if (process.env.REACT_APP_DB_TYPE === process.env.REACT_APP_MYSQL) {
      console.log("MYSQL root url");
      this.rootUrl = process.env.REACT_APP_ROOT_URL_MYSQL;
    }

    axios.get(this.rootUrl + '/')
      .then(res => {
        console.log(res.data);
        if ((!res.data || res.data.length === 0) &&
          process.env.REACT_APP_DB_TYPE === process.env.REACT_APP_MONGO) {
          console.log("No data loaded from MongoDb.");
          let rooms = [...this.state.rooms];
          for (let room of this.seedForEmptyMongo) {
            axios.post(this.rootUrl + '/add', room)
              .then(res => {
                console.log(res.data);
                rooms.push(res.data);
                this.setState({ rooms: rooms });
              });
          }
        }
        else {
          this.setState({ rooms: res.data })
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  getRooms = () => {
    return this.state.rooms;
  }

  updateRooms = (rooms, updRoom, arrayProp, subDoc, changedProp, type) => {
    let reqObj = {};
    if (updRoom) reqObj.room = updRoom;
    if (arrayProp) reqObj.arrayProp = arrayProp;
    if (subDoc) reqObj.subDoc = subDoc;
    if (changedProp) reqObj.prop = changedProp;
    if (type) reqObj.type = type;
    axios.put(this.rootUrl + '/update/' + updRoom._id, reqObj)
      .then(res => {
        console.log(res.data);
        this.setState({ rooms: rooms });
      });
  }

  onAddRoom = () => {
    let rooms = [...this.state.rooms];
    let room = { name: "Kinosál " + (rooms.length + 100), seats: this.initializeSeats(6), films: [...filmData] }
    axios.post(this.rootUrl + '/add', room)
      .then(res => {
        console.log(res.data);
        rooms.push(res.data);
        this.setState({ rooms: rooms });
      });
  }

  onDeleteRoom = (_id) => {
    axios.delete(this.rootUrl + '/' + _id)
      .then(res => {
        console.log(res.data);
        this.setState({ rooms: this.state.rooms.filter(r => r._id !== _id) });
      });
  }

  editRoomName = (room_id, value) => {
    let rooms = [...this.state.rooms];
    let room = rooms.find(r => r._id === room_id);
    room.name = value;
    this.updateRooms(rooms, room);
  }

  render() {
    return (
      <div>
        <h1><u>Kino Skladon Cinema</u></h1>
        <p>
          Černá: Volno<br />Zelená: Rezervováno<br />
          <button onClick={() => this.onAddRoom()}>Add new room</button>
        </p>
        {
          this.state.rooms.map(room => (
            <Room
              key={uuid.v4()}
              getRooms={this.getRooms}
              updateRooms={this.updateRooms}
              room={room}
              onDelete={this.onDeleteRoom}
              editName={this.editRoomName}
            />
          ))
        }
      </div >
    );
  }
}
export default Cinema;