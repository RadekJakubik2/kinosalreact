import React, { Component } from "react";
import "./App.css";
import EditableLabel from 'react-editable-label';
import PropTypes from 'prop-types';

class Film extends Component {
  render() {
    const { film, room, onDelete, editTitle, editDescription } = this.props;
    return (
      <div>
        <div className="box">
          <b className="box">
            <EditableLabel
              initialValue={film.title}
              save={value => {
                if (film.title !== value) {
                  console.log(`Saving '${value}'`);
                  editTitle(room._id, film._id, value);
                }
              }}
            />
          </b>
        </div>
        <label className="box rightPaddingLabel"> : </label>
        <div className="box">
          <EditableLabel
            initialValue={film.description}
            save={value => {
              if (film.description !== value) {
                console.log(`Saving '${value}'`);
                editDescription(room._id, film._id, value);
              }
            }}
          />
        </div>
        <button className="box" onClick={() => onDelete(room._id, film._id)}>
          Delete
        </button>
      </div>
    );
  }
}

Film.propTypes = {
  film: PropTypes.object,
  room: PropTypes.object,
  onDelete: PropTypes.func,
  editTitle: PropTypes.func,
  editDescription: PropTypes.func
};

export default Film;