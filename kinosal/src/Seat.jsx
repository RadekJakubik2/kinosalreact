import React, { Component } from "react";
import uuid from 'uuid';
import "./App.css";
import PropTypes from 'prop-types';

class Seat extends Component {

  render() {
    const { seat, room, onClick } = this.props;
    const className = (seat.reserved) ? "reserved-button button" : "button";
    return (
      <button className={className} onClick={() => onClick(room._id, seat._id)}
        key={uuid.v4()} value={seat.name}>{seat.name}</button>
    );
  }
}

Seat.propTypes = {
  seat: PropTypes.object,
  room: PropTypes.object,
  onClick: PropTypes.func
};

export default Seat;