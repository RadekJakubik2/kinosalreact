const asyncMiddleware = require('../middleware/async');
const router = require('express').Router();
let RoomModel = require('../models/RoomModel-mongo');
require('../models/dbMongo.js');

router.get('/', asyncMiddleware(async (req, res) => {
  const rooms = await RoomModel.find()
    .select("-__v")
    .sort("name");
  res.send(rooms);
}));

router.post('/add', asyncMiddleware(async (req, res) => {
  const name = req.body.name;
  const seats = req.body.seats;
  const films = req.body.films;

  const newRoom = new RoomModel({
    name,
    seats,
    films,
  });

  await newRoom.save();
  res.send(newRoom);
}));

router.put('/update/:id', asyncMiddleware(async (req, res) => {
  const roomReq = req.body['room'];
  const arrayProp = req.body["arrayProp"];
  const finalRequest = {};
  if (!arrayProp) finalRequest.name = roomReq.name;
  else if (arrayProp === "seats") finalRequest.seats = roomReq.seats;
  else if (arrayProp === "films") finalRequest.films = roomReq.films;

  const room = await RoomModel.findByIdAndUpdate(
    req.params.id,
    finalRequest,
    { new: true }
  );
  res.send(room);
}));

router.delete('/:id', asyncMiddleware(async (req, res) => {
  await RoomModel.findByIdAndDelete(req.params.id);
  res.send(req.params.id);
}));

module.exports = router;