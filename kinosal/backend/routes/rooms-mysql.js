const asyncMiddleware = require('../middleware/async');
const router = require('express').Router();
const ConnectionMySql = require('../models/dbMySql');

function simpleQuery(baseQuery, values) {
  let resQuery = ConnectionMySql.format(baseQuery, values);
  ConnectionMySql.query(resQuery
    , (err, result) => {
      if (err) { console.log("error: ", err); }
      else { /*console.log(result);*/ }
    });
}

async function promiseQuery(baseQuery, values, returnOnlyId) {
  let resQuery = ConnectionMySql.format(baseQuery, values);
  return await new Promise((resolve, reject) => {
    ConnectionMySql.query(
      resQuery
      , (err, result) => {
        if (err) {
          console.log(err);
          return reject(err);
        }
        if (returnOnlyId) resolve(result.insertId);
        else resolve(result);
      });
  });
}

extractRooms = (result) => {
  let rooms = [];
  for (let i = 0; i < result.length; i++) {
    if (!rooms.find(r => r._id === result[i].room_id))
      rooms.push({ _id: result[i].room_id, name: result[i].room_name, seats: [], films: [] });
  }
  return rooms;
}
extractSeats = (result, rooms) => {
  for (let i = 0; i < result.length; i++) {
    let room = rooms.find(r => r._id === result[i].room_id);
    if (room) {
      room.seats.push({ _id: result[i].seat_id, name: result[i].seat_name, reserved: result[i].seat_reserved });
    }
  }
}
extractFilms = (result, rooms) => {
  for (let i = 0; i < result.length; i++) {
    let room = rooms.find(r => r._id === result[i].room_id);
    if (room) {
      room.films.push({ _id: result[i].film_id, title: result[i].film_title, description: result[i].film_description });
    }
  }
}

const getRoomsAndSeats =
  "SELECT room.id as room_id, room.name as room_name,"
  + " seat.id as seat_id,  seat.name as seat_name, seat.reserved as seat_reserved"
  + " FROM room JOIN seat ON room.id = seat.room_id";
const getRoomsAndFilms =
  "SELECT room.id as room_id, room.name as room_name,"
  + " film.id as film_id, film.title as film_title, film.description as film_description"
  + " FROM room JOIN film ON room.id = film.room_id";

async function fetchAllData(queryObj1, queryObj2) {
  const outcomeRoomsSeats = await promiseQuery(queryObj1.base, queryObj1.values, queryObj1.retId);
  const outcomeRoomsFilms = await promiseQuery(queryObj2.base, queryObj2.values, queryObj2.retId);
  let rooms = extractRooms(outcomeRoomsSeats);
  extractSeats(outcomeRoomsSeats, rooms);
  extractFilms(outcomeRoomsFilms, rooms);
  return rooms;
}

router.get('/', asyncMiddleware(async (req, res) => {
  const queryObj1 = { base: getRoomsAndSeats, values: [], retId: false };
  const queryObj2 = { base: getRoomsAndFilms, values: [], retId: false };
  let rooms = await fetchAllData(queryObj1, queryObj2);
  res.send(rooms);
}));

router.post('/add', asyncMiddleware(async (req, res) => {
  const name = req.body.name;
  const seats = req.body.seats;
  const films = req.body.films;

  const roomQueryParts = {
    query: "INSERT INTO ?? (??) VALUES (?)",
    values: ['room', 'name', name]
  };
  let newId = await promiseQuery(roomQueryParts.query, roomQueryParts.values, true);
  const querries = [];
  for (let seat of seats) {
    querries.push({
      base: "INSERT INTO ?? (??,??,??) VALUES (?,?,?)",
      values: ['seat', 'name', 'reserved', 'room_id', seat.name, seat.reserved, newId]
    });
  }
  for (let film of films) {
    querries.push({
      base: "INSERT INTO ?? (??,??,??) VALUES (?,?,?)",
      values: ['film', 'title', 'description', 'room_id', film.title, film.description, newId]
    });
  }

  for (let query of querries) {
    simpleQuery(query.base, query.values);
  }
  // return inserted object back to the view
  const queryObj1 = { base: getRoomsAndSeats + " WHERE room.id = ?", values: [newId], retId: false };
  const queryObj2 = { base: getRoomsAndFilms + " WHERE room.id = ?", values: [newId], retId: false };
  const rooms = await fetchAllData(queryObj1, queryObj2);
  res.send(rooms[0]);
}));

router.put('/update/:id', asyncMiddleware(async (req, res) => {
  const roomReq = req.body['room'];
  const arrayProp = req.body["arrayProp"];
  const subDoc = req.body["subDoc"];
  const changedProp = req.body["prop"];
  let baseQuery;
  let values;
  if (req.body["type"] === "delete") {
    //smazat film
    baseQuery = "DELETE FROM ?? WHERE ?? = ?";
    values = ['film', 'id', subDoc._id];
  }
  else if (!arrayProp) {
    baseQuery = "UPDATE ?? SET ?? = ? WHERE ?? = ?";
    values = ['room', 'name', roomReq.name, 'id', req.params.id];
  }
  else if (arrayProp === "seats") {
    baseQuery = "UPDATE ?? SET ?? = ? WHERE ?? = ?";
    values = ['seat', changedProp, subDoc[changedProp], 'id', subDoc._id];
  }
  else if (arrayProp === "films") {
    baseQuery = "UPDATE ?? SET ?? = ? WHERE ?? = ?";
    values = ['film', changedProp, subDoc[changedProp], 'id', subDoc._id];
  }

  simpleQuery(baseQuery, values);
  res.send(JSON.stringify(req.body));
}));

//smazání room
router.delete('/:id', asyncMiddleware(async (req, res) => {
  const querries = [];
  querries.push({ base: "DELETE FROM ?? WHERE ?? = ?", values: ['room', 'id', req.params.id] });
  querries.push({ base: "DELETE FROM ?? WHERE ?? = ?", values: ['seat', 'room_id', req.params.id] });
  querries.push({ base: "DELETE FROM ?? WHERE ?? = ?", values: ['film', 'room_id', req.params.id] });

  for (let queryObj of querries) {
    simpleQuery(queryObj.base, queryObj.values);
  };

  res.send(JSON.stringify(req.body));
}));

module.exports = router;