const mongoose = require('mongoose');

const uri = process.env.REACT_APP_MONGO_URI;

mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true }, (err, db) => {
  if (err) {
    console.log('Unable to connect to the server. Please start the server. Error:', err);
  } else {
    console.log('Connected to Server successfully!');
  }
});

const connectionMongo = mongoose.connection;
connectionMongo.once('open', () => {
  console.log("MongoDB database connection established successfully");
})