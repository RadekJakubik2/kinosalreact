const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const filmSchema = new Schema({
  title: String,
  description: String,
});

const seatSchema = new Schema({
  name: String,
  reserved: Boolean
});

const roomSchema = new Schema({
  name: String,
  seats: [seatSchema],
  films: [filmSchema]
});
const RoomModel = mongoose.model('Room', roomSchema);

module.exports = RoomModel;