var mysql = require('mysql');

const config = {
  host: process.env.REACT_APP_MYSQL_HOST,
  port: process.env.REACT_APP_MYSQL_PORT,
  user: process.env.REACT_APP_MYSQL_USER,
  password: process.env.REACT_APP_MYSQL_PASSWORD,
  database: process.env.REACT_APP_MYSQL_DATABASE
}

const ConnectionMySql = mysql.createConnection(config);
ConnectionMySql.connect(err => {
  if (err) {
    console.log(err);
  }
  else {
    console.log("MySQL database connection established successfully");
  }
});

module.exports = ConnectionMySql;