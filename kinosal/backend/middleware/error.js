const winston = require('winston');

module.exports = function (err, req, res, next) {
  winston.error(err.message, err);
  console.log(res.statusCode);
  res.status(500).send();
}