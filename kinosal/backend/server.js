const express = require('express');
const winston = require('winston');
const cors = require('cors');
const error = require('./middleware/error');
require('dotenv').config();
const app = express();

process.on('uncaughtException', (ex) => {
  winston.error(ex.message, ex);
  process.exit(1);
});
process.on('unhandledRejection', (ex) => {
  winston.error(ex.message, ex);
  process.exit(1);
});
winston.add(winston.transports.File, { filename: 'logfile.log' });

const port = process.env.REACT_APP_PORT || 5000;
app.use(cors());
app.use(express.json());

if (process.env.REACT_APP_DB_TYPE === process.env.REACT_APP_MONGO) {
  console.log("MONGO db");
  const roomsRouter = require('./routes/rooms-mongo');
  app.use('/rooms-mongo', roomsRouter);
}
else if (process.env.REACT_APP_DB_TYPE === process.env.REACT_APP_MYSQL) {
  console.log("MYSQL db");
  const roomsRouter = require('./routes/rooms-mysql');
  app.use('/rooms-mysql', roomsRouter);
}
app.use(error);

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});
